//
//  HelpersAssembly.swift
//  ios-loonify
//
//  Created by Oleg Kulakevich on 05.08.2020.
//  Copyright © 2020 Loonify. All rights reserved.
//

import Foundation
import Swinject

class HelpersAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(NetworkController.self) { r in
            let networkController = NetworkController(base: API_BASE, trustPolicies: [:])
            return networkController
        } .inObjectScope(.container)
    }
}
