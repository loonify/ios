//
//  ErrorStorage.swift
//  ios-loonify
//
//  Created by Oleg Kulakevich on 05.08.2020.
//  Copyright © 2020 Loonify. All rights reserved.
//

import Foundation
import Alamofire

public enum AppError: Error {
    case internalError
}

public enum LoonifyCallError: Error {
    case httpError(Int?, String?)
    case serverError(String?)
    case notFound(String?)
    case unprocessable
    case methodNotAllowed
    case basketError
    
    init(statusCode: Int, message: String? = nil) {
        switch statusCode {
        case 422:
            self = .unprocessable
        case 424:
            self = .basketError
        case 404:
            self = .notFound(message)
        case 405:
            self = .methodNotAllowed
        case 500:
            self = .serverError(message)
        default:
            self = .httpError(statusCode, message)
        }
    }
}


