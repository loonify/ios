//
//  Router.swift
//  ios-loonify
//
//  Created by Oleg Kulakevich on 05.08.2020.
//  Copyright © 2020 Loonify. All rights reserved.
//

import Foundation
import Alamofire

public struct Router {
    private let base: String
    
    public init(base: String) {
        self.base = base
    }
    
    public func baseRoute(path: String, method: HTTPMethod, parameters: Parameters, headers: [String: String]? = nil, httpBody: Data? = nil) -> Route {
        return Route(base: base, path: path, method: method, parameters: parameters, headers: headers, httpBody: httpBody)
    }
}
