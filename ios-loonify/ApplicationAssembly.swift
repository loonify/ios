//
//  ApplicationAssembly.swift
//  ios-loonify
//
//  Created by Oleg Kulakevich on 05.08.2020.
//  Copyright © 2020 Loonify. All rights reserved.
//

import Swinject
import SwinjectStoryboard

fileprivate var assembler: Assembler!

extension SwinjectStoryboard {
    
    @objc
    class func setup() {
        Container.loggingFunction = nil
        
        let assemblies: [Assembly] = [
            HelpersAssembly(),
            
            WelcomeAssembly(),
        ]
        
        assembler = Assembler(assemblies, container: defaultContainer)
    }
}
