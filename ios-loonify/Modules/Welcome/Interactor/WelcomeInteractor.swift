//
//  WelcomeWelcomeInteractor.swift
//  ios-loonify
//
//  Created by Oleg Kulakevich on 05/08/2020.
//  Copyright © 2020 Loonify. All rights reserved.
//

import Foundation

final class WelcomeInteractor: WelcomeInteractorInput {

    weak var output: WelcomeInteractorOutput?

}
