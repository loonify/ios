//
//  WelcomeWelcomeModuleInput.swift
//  ios-loonify
//
//  Created by Oleg Kulakevich on 05/08/2020.
//  Copyright © 2020 Loonify. All rights reserved.
//

import Foundation

protocol WelcomeModuleInput: class {

	func configure()
	
}
