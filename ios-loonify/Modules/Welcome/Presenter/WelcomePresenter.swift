//
//  WelcomeWelcomePresenter.swift
//  ios-loonify
//
//  Created by Oleg Kulakevich on 05/08/2020.
//  Copyright © 2020 Loonify. All rights reserved.
//

import Foundation
import ViperArch

final class WelcomePresenter: ModuleInput, WelcomeModuleInput, WelcomeViewOutput, WelcomeInteractorOutput {

    weak var view: WelcomeViewInput?
    var interactor: WelcomeInteractorInput?
    var router: WelcomeRouterInput?

    internal func viewIsReady() {
        view?.setupInitialState()
    }

    func set(moduleOutput: ModuleOutput) {
    }

    func configure() {
    }

    //MARK: WelcomeViewOutput

    //MARK: WelcomeInteractorOutput
}
