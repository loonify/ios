//
//  WelcomeWelcomeAssembly.swift
//  ios-loonify
//
//  Created by Oleg Kulakevich on 05/08/2020.
//  Copyright © 2020 Loonify. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

final class WelcomeAssembly: Assembly {

    func assemble(container: Container) {

        container.storyboardInitCompleted(WelcomeViewController.self) { (r, view) in
            view.output = r.resolve(WelcomePresenter.self, argument: view)
        }

        container.register(WelcomeViewController.self) { r in
            let view = WelcomeViewController()
            view.output = r.resolve(WelcomePresenter.self, argument: view)
            return view
        }

        container.register(WelcomePresenter.self) { (r, view: WelcomeViewController) in
            let presenter = WelcomePresenter()
            let interactor = r.resolve(WelcomeInteractor.self, argument: presenter)!
            let router = r.resolve(WelcomeRouter.self, arguments: view, interactor)!
            presenter.view = view
            presenter.interactor = interactor
            presenter.router = router
            return presenter
        }

        container.register(WelcomeInteractor.self) { (r, presenter: WelcomePresenter) in
            let interactor = WelcomeInteractor()
            interactor.output = presenter
            return interactor
        }

        container.register(WelcomeRouter.self) { (r, view: WelcomeViewController, interactor: WelcomeInteractor) in
            let router = WelcomeRouter()
            router.interactor = interactor
            router.transitionHandler = view
            return router
        }

    }

}
