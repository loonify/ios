//
//  WelcomeWelcomeViewController.swift
//  ios-loonify
//
//  Created by Oleg Kulakevich on 05/08/2020.
//  Copyright © 2020 Loonify. All rights reserved.
//

import UIKit

final class WelcomeViewController: UIViewController, WelcomeViewInput {

    var output: WelcomeViewOutput?

    // MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewIsReady()
    }

    // MARK: WelcomeViewInput

    func setupInitialState() {
    }

}
