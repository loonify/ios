//
//  Constants.swift
//  ios-loonify
//
//  Created by Oleg Kulakevich on 05.08.2020.
//  Copyright © 2020 Loonify. All rights reserved.
//

import Foundation

// =====================================
// MARK: Debug
// =====================================
let NETWORK_DEBUG = false
let APP_DEBUG = false
// =====================================
// MARK: Network client
// =====================================
let API_BASE = ""
